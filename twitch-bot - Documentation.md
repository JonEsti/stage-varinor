# Twitch Bot
## Bien démarrer
Nous allons utiliser Node.JS afin de réaliser le Varibot. En effet, celui-ci excelle dans le traitement de données légères en temps réel.

Commencez donc par installer Node.JS en version 4.X (important pour la suite). Il existe des installeurs pour Mac et Windows. Sous Linux, entrez la commande :
```
sudo apt-get install -y nodejs
```
Afin de vérifier que Node est bien installé, entrez la commande :
```
node -v
```

On créez maintenant un dossier qui va contenir notre bot et rendez-vous dedans avec une console.

Node est paqueté avec le gestionnaire de paquet NPM. Ce dernier va nous permettre d'initialiser notre projet avec la commande suivante :
```
npm init
```
En suivant les instructions, NPM vous créera le fichier Package.json qui contiendra des informations essentielles pour le projet.

Il est temps d'installer la librairie qui sera au coeur de notre bot, tmi.js (Twitch Messaging Interface) à l'aide de la commande :
```
npm install tmi.js --save
```
Nous avons maintenant toutes les dépendances nécessaires à notre projet. La prochaine étape est simple. Notre bot a besoin d'un compte pour poster sur Twitch.tv ! On créer un compte basique "Varibot".

Enfin, Twitch utilise IRC pour son chat. Notre application a donc besoin des identifiants du bot pour parler en son nom. Lui donner directement le mot de passe n'est évidemment pas très sécurisant. Twitch exige donc de passer par le système OAuth. Pour nous simplifier la vie, il existe une application effectuant le travail en un clique.

Visitez l'adresse http://twitchapps.com/tmi/ et cliquez sur "Connect with Twitch" en étant connecté avec le compte Twitch de votre bot. L'application va alors vous retourner un code commençant par "oauth:" suivi d'une longue série de caractères. Copiez ce token et mettez le de côté pour l'instant.


## Bonjour Twitch !
 On créer un fichier twitch-bot.js à la racine de votre projet, et on y ajoute  la ligne suivante :
 ```
 const tmi = require('tmi.js');
 ```
 Cette ligne va importer la librairie TMI dans notre application.

A noter l'utilisation de "const". J'utilise en effet la spécification ECMAScript 2015 (ES6) de Javascript. Bien entendu, vous pouvez utiliser ES5 et cela fonctionnera parfaitement, en remplaçant par exemple "const" par "var". Cependant, Node.JS la supportant en grande partie (Voir http://node.green pour l'avancement de l'intégration d'ES6), pourquoi s'en passer ? A noter également que nous utilisons ici la version 4.x de Node et non la dernière (6.x aujourd'hui).

Configurons maintenant TMI.js afin de se connecter à notre chaîne Twitch. Ajoutez donc l'objet suivant à twitch-bot.js :
```
const tmiConfig = {
    options: {
        debug: true
    },
    connection: {
        reconnect:  true
    },
    identity: {
        username: "Le nom d'utilisateur de votre bot Twitch",
        password: "Le token oauth que vous avez sauvegardé précédement"
    },
    channels: [
        "le nom de la chaine twitch que le bot doit rejoindre"
    ]
};
```
Puis la ligne :
```
let client = new tmi.client(tmiConfig);
```
Détaillons un peu les paramètres que nous passons. La propriété "debug" affiche simplement les messages de type debug dans la console. Avec "reconnect" à true, le bot tentera de se reconnecter au chat si il perd la connexion à un moment. Les autres éléments sont déjà détaillés par leur valeur. On retrouve donc le nom d'utilisateur du bot et le token oauth. Enfin "channels" est un tableau de string représentant le nom des chaînes Twitch que le bot doit rejoindre. Vous pouvez bien entendu retrouver la liste complète des paramètres à l'adresse https://docs.tmijs.org/v1.1.1/Configuration.html.

TMI a donc tous les éléments pour se connecter à notre chaîne. Il ne reste plus qu'à lui en donner l'ordre en rajoutant :
```
client.connect();
```
Sauvegardez le fichier et dans votre console, naviguez jusqu'au répertoire du projet et lancez Node avec :
```
node bot.js
```
Votre bot est vivant ! Rendez vous sur le chat de votre chaîne, cliquez sur l'icône "Liste des viewers" en bas à gauche et.... magie ! Votre bot est là.

Bon pour l'instant il ne fait pas grand chose, mais il est vivant !

## Interactions et commandes

Créons notre première commande. Elles se compose généralement d'un préfixe et d'un mot clé. Par exemple, je peux souhaiter que lorsque un viewer envoie :
```
!bonjour
```
Le bot lui réponde "Bonjour à toi @nomUtilisateur !". Tout d'abord, il nous faut définir notre préfixe. Rajoutez en haut de bot.js 
```
const prefix = "!";
```
Ensuite, il nous faut une fonction capable de parser les messages entrant à la recherche d'un format de type commande. On va premièrement utiliser une Regex afin de d'abord échapper le préfixe. En effet, si on utilise par exemple "!" en tant que préfixe et que ensuite on le met dans notre Regex, cela va causer des problèmes. Ensuite, on recherche donc un message ayant le format "préfixe suivi d'un mot suivi d'un espace éventuel suivi d'un paramètre éventuel (n'importe quoi)". Voici donc la fonction à ajouter dans notre fichier :
```
function commandParser(message){
    let prefixEscaped = prefix.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    let regex = new RegExp("^" + prefixEscaped + "([a-zA-Z]+)\s?(.*)");
    return regex.exec(message);
}
```
Cette fonction va nous retourner un tableau contenant la commande et le paramètre. On va ensuite utiliser un switch afin de déterminer si la commande demandée est connue ou non. Voici donc la fonction de l'évènement chat modifiée :
```
client.on('chat', (channel, user, message, isSelf) => {
    if (isSelf) return;

    let fullCommand = commandParser(message);
    
    if(fullCommand){
        let command = fullCommand[1];
        let param = fullCommand[2];
        
        switch(command){
            case "bonjour":
                client.say(channel, "Bonjour à toi " + user['display-name']);
                break;
            default:
                client.say(channel, "Commande '" + command + "'' non reconnue. Tapez " + prefix + "help pour la liste des commandes de " + client.getUsername());
        }
    }
});
```
## Système de réactions du bot

Les commandes utilisent donc un préfixe afin de ne pas être déclenchées par mégarde. Cependant, les réactions n'en ont pas. Cette fonctionnalité est souvent utilisées à des fins amusantes en utilisant des réactions personnalisées. Il suffit donc de créer un fichier reactions.js à la racine contenant un JSON de clé valeur sous la forme "mot déclencheur" : "phrase à envoyer". Par exemple :
```
const reactions = {
    "camion" : "POUEET POUEEEET",
    "oui" : "Stiti !!"
}
module.exports = reactions;
```
En haut de twitch-bot.js, on ajoutera le "require" :
```
const reactions = require('./reactions.js');
```
Enfin, pour chaque message qui n'aura pas déclenché de commande, nous allons séparer chaque mot du message et vérifier si ce mot est une clé de notre JSON de réactions. Si c'est le cas, nous enverrons la phrase correspondante.
```
let words = message.split(" ");
for(let word of words) {
    let reaction = reactions[word];
    if(reaction){
        client.say(channel, reaction);
    }
}
```
## Message automatique

Le Varibot devra de temps en temps partager les liens vers les différents réseaux sociaux de la chaine nous allons faire ça avec cette commande:
```
setInterval( ()=> {
    client.say("Varinnor", "Yo! Tu peux me retrouver ici -> Twitter: https://twitter.com/Varinor ; Youtube: https://www.youtube.com/channel/UCX6E9EI2fx8oaPFuVFwIhaw ; Discord: https://discordapp.com/invite/UaXqSy91");
     }, 120000);
```

Source : *https://www.supinfo.com/articles/single/2699-realisez-bot-twitch-nodejs*